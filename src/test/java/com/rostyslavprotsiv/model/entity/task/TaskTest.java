package com.rostyslavprotsiv.model.entity.task;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {
    private static Task task;

    @BeforeAll
    static void setUp() {
        task = Task.getInstance("do smth");
    }

    @Test
    void testWorkflow() {
        String testStr = "ttt";
        task.startSprint();
        task.executeTask();
        task.changeTask(testStr);
//        task.test();
        task.startSprint();
        assertTrue(task.toString().contains(testStr));
    }
}