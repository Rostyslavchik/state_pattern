package com.rostyslavprotsiv.model.entity.task;

import com.rostyslavprotsiv.model.entity.state.IState;
import com.rostyslavprotsiv.model.entity.state.ProductBacklogState;

/**
 * This {@code Task} is Double-Checked locking realization
 *
 * @author Rostyslav
 * @since 29.09.2021
 * @version 1.0.0
 */
public class Task {
    private IState state;
    private String todo;
//    private static Task instance;
    private static volatile Task instance;

    private Task(String todo) {
        this.todo = todo;
        state = new ProductBacklogState();
    }

    public static Task getInstance(String todo) {
        if (instance == null) {
            synchronized (Task.class) {
                if (instance == null) {
                    instance = new Task(todo);
                }
            }
        }
        return instance;
    }

    public void setState(IState state) {
        this.state = state;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public void changeTask(String newTask) {
        state.changeTask(this, newTask);
    }

    public void toProductBacklog() {
        state.toProductBacklog(this);
    }

    public void startSprint() {
        state.startSprint(this);
    }

    public void executeTask() {
        state.executeTask(this);
    }

    public void test() {
        state.test(this);
    }

    public void review() {
        state.review(this);
    }

    public void finish() {
        state.finish(this);
    }

    public void block() {
        state.block(this);
    }

    @Override
    public String toString() {
        return "Task{" +
                "state=" + state +
                ", todo='" + todo + '\'' +
                '}';
    }
}
