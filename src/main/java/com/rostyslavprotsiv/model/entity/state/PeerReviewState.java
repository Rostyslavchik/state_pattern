package com.rostyslavprotsiv.model.entity.state;

import com.rostyslavprotsiv.model.entity.task.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PeerReviewState implements IState {
    private static final Logger LOGGER = LogManager.getLogger(
            PeerReviewState.class);

    @Override
    public void executeTask(Task task) {
        task.setState(new InProgressState());
        LOGGER.info("Task is in progress again after review");
    }

    @Override
    public void block(Task task) {
        task.setState(new BlockedState());
        LOGGER.info("Task is blocked");
    }

    @Override
    public void finish(Task task) {
        task.setState(new DoneState());
        LOGGER.info("Congratulations! Task is finished");
    }
}
