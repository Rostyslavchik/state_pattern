package com.rostyslavprotsiv.model.entity.state;

import com.rostyslavprotsiv.model.entity.task.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InProgressState implements IState {
    private static final Logger LOGGER = LogManager.getLogger(
            InProgressState.class);

    @Override
    public void changeTask(Task task, String newTask) {
        task.setTodo(newTask);
        task.setState(new ProductBacklogState());
        LOGGER.info("Task was changed");
    }

    @Override
    public void test(Task task) {
        task.setState(new InTestState());
        LOGGER.info("Task is being tested");
    }

    @Override
    public void review(Task task) {
        task.setState(new PeerReviewState());
        LOGGER.info("Task is under peer review");
    }

    @Override
    public void block(Task task) {
        task.setState(new BlockedState());
        LOGGER.info("Task is blocked");
    }
}
