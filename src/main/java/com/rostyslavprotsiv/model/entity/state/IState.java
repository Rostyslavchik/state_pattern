package com.rostyslavprotsiv.model.entity.state;

import com.rostyslavprotsiv.model.entity.task.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface IState {
    default void changeTask(Task task, String newTask) {
        throw new UnsupportedOperationException(
                "You can't change task from this state"
                        + this.getClass().getSimpleName());
    }

    default void toProductBacklog(Task task) {
        throw new UnsupportedOperationException(
                "Moving to Product Backlog state is not supported by this state"
                        + this.getClass().getSimpleName());
    }

    default void startSprint(Task task) {
        throw new UnsupportedOperationException(
                "Moving to Sprint Backlog state is not supported by this state"
                        + this.getClass().getSimpleName());
    }

    default void executeTask(Task task) {
        throw new UnsupportedOperationException(
                "Moving to In Progress state is not supported by this state"
                        + this.getClass().getSimpleName());
    }

    default void test(Task task) {
        throw new UnsupportedOperationException(
                "Moving to In Test state is not supported by this state"
                        + this.getClass().getSimpleName());
    }

    default void review(Task task) {
        throw new UnsupportedOperationException(
                "Moving to Peer Review state is not supported by this state"
                        + this.getClass().getSimpleName());
    }

    default void finish(Task task) {
        throw new UnsupportedOperationException(
                "Moving to Done state is not supported by this state"
                        + this.getClass().getSimpleName());
    }

    default void block(Task task) {
        throw new UnsupportedOperationException(
                "Moving to Block state is not supported by this state"
                        + this.getClass().getSimpleName());
    }
}
