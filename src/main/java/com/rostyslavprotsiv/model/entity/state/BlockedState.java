package com.rostyslavprotsiv.model.entity.state;

import com.rostyslavprotsiv.model.entity.task.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BlockedState implements IState {
    private static final Logger LOGGER = LogManager.getLogger(
            BlockedState.class);

    @Override
    public void changeTask(Task task, String newTask) {
        task.setTodo(newTask);
        task.setState(new ProductBacklogState());
        LOGGER.info("Task was changed");
    }

    @Override
    public void toProductBacklog(Task task) {
        task.setState(new ProductBacklogState());
        LOGGER.info("Task is in Product Backlog again");
    }
}
