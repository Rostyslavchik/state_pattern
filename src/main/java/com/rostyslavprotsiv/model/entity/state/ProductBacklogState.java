package com.rostyslavprotsiv.model.entity.state;

import com.rostyslavprotsiv.model.entity.task.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProductBacklogState implements IState {
    private static final Logger LOGGER = LogManager.getLogger(
            ProductBacklogState.class);

    @Override
    public void changeTask(Task task, String newTask) {
        task.setTodo(newTask);
        LOGGER.info("Task was changed");
    }

    @Override
    public void startSprint(Task task) {
        task.setState(new SprintBacklogState());
        LOGGER.info("Starting the sprint");
    }
}
