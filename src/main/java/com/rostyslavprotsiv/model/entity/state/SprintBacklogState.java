package com.rostyslavprotsiv.model.entity.state;

import com.rostyslavprotsiv.model.entity.task.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SprintBacklogState implements IState {
    private static final Logger LOGGER = LogManager.getLogger(
            SprintBacklogState.class);

    @Override
    public void changeTask(Task task, String newTask) {
        task.setTodo(newTask);
        task.setState(new ProductBacklogState());
        LOGGER.info("Task was changed");
    }

    @Override
    public void executeTask(Task task) {
        task.setState(new InProgressState());
        LOGGER.info("Task is in progress...");
    }

    @Override
    public void block(Task task) {
        task.setState(new BlockedState());
        LOGGER.info("Task is blocked");
    }
}
