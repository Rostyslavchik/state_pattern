package com.rostyslavprotsiv.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);
    private static Map<String, String> menu = new LinkedHashMap<>();
    private static final Scanner SCANNER = new Scanner(System.in);

    public Menu() {
        menu.put("1", "Change task");
        menu.put("2", "Go to Product Backlog");
        menu.put("3", "Go to Sprint Backlog");
        menu.put("4", "Go to task execution");
        menu.put("5", "Go to Peer Review");
        menu.put("6", "Go to Testing");
        menu.put("7", "Go to Finishing");
        menu.put("8", "Go to Blocking");
        menu.put("G", "Get Task");
        menu.put("Q", "Quit");
    }

    public void welcome() {
        LOGGER.info("Welcome to my program!");
    }

    public String outMenuAndGetInput() {
        menu.forEach((i, s) -> LOGGER.info(i + "-" + s));
        return SCANNER.nextLine();
    }

    public String inputNewTask() {
        LOGGER.info("Please, input your new task");
        return SCANNER.nextLine();
    }

    public void outObject(String obj) {
        LOGGER.info(obj);
    }
}
