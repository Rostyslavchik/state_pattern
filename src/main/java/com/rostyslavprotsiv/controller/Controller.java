package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.entity.task.Task;
import com.rostyslavprotsiv.view.Menu;

public class Controller {
    private static final Menu MENU = new Menu();
    private static Task task = Task.getInstance("do smth");

    public void execute() {
        MENU.welcome();
        testStatePattern();
    }

    private void testStatePattern() {
        String keyMenu;
        do {
            keyMenu = MENU.outMenuAndGetInput().toUpperCase();
            try {
                switch (keyMenu) {
                    case "1":
                        task.changeTask(MENU.inputNewTask());
                        break;
                    case "2":
                        task.toProductBacklog();
                        break;
                    case "3":
                        task.startSprint();
                        break;
                    case "4":
                        task.executeTask();
                        break;
                    case "5":
                        task.review();
                        break;
                    case "6":
                        task.test();
                        break;
                    case "7":
                        task.finish();
                        break;
                    case "8":
                        task.block();
                        break;
                    case "G":
                        MENU.outObject(task.toString());
                        break;
                    default:
                        throw new UnsupportedOperationException("Bad input!!");
                }
            } catch (UnsupportedOperationException e) {
                System.err.println(e.getMessage());
            }
        } while (!keyMenu.equals("Q"));
    }
}
